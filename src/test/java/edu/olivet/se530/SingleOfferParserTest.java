package edu.olivet.se530;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.olivet.se530.dummy.DummyHtmlCrawler;
import edu.olivet.se530.model.Condition;

public class SingleOfferParserTest {
	static Elements offerRows;
	static Element commonOffer;
	@BeforeClass
	public static void init() throws MalformedURLException, IOException {
		HtmlCrawler crawler = new DummyHtmlCrawler();
		Document doc = crawler.getDocument("135157862", "new");
		offerRows = doc.select("div.a-row.a-spacing-mini.olpOffer");
		commonOffer = offerRows.get(1);
	}
	
	@Test
	public void testParsePrice() {
		assertEquals(Float.valueOf("46.38"), SingleOfferParser.parsePrice(commonOffer));
	}
	
	@Test
	public void testparseShippingPrice() {
		assertEquals(Float.valueOf("3.99"), SingleOfferParser.parseShippingPrice(commonOffer));
	}

	@Test
	public void testParseCondition() {
		Condition cond = SingleOfferParser.parseCondition(commonOffer);
		assertEquals("New", cond.getPrimary());	
	}

}
