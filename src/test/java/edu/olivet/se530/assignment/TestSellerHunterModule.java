package edu.olivet.se530.assignment;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

import edu.olivet.se530.HtmlCrawler;
import edu.olivet.se530.annotations.Profile;
import edu.olivet.se530.aop.ProfileIntecepter;
import edu.olivet.se530.dummy.DummyHtmlCrawler;

public class TestSellerHunterModule extends AbstractModule {

	@Override
	protected void configure() {
		// TODO Auto-generated method stub
		this.bind(HtmlCrawler.class).to(DummyHtmlCrawler.class);
		ProfileIntecepter intecepter = new ProfileIntecepter();
		this.bindInterceptor(Matchers.any(),
				Matchers.annotatedWith(Profile.class), intecepter);
	}

}
