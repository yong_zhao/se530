package edu.olivet.se530;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jukito.JukitoRunner;
import org.jukito.UseModules;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;

import edu.olivet.se530.dummy.DummyHtmlCrawler;
import edu.olivet.se530.model.Offer;
import edu.olivet.se530.modules.ProfileModule;

@RunWith(JukitoRunner.class)
@UseModules(value = ProfileModule.class)
public class HtmlParserTest {
	@Inject private HtmlParser htmlParser;
	private static Document document;
	
	@BeforeClass public static void prepareDocument() throws MalformedURLException, IOException {
		document = new DummyHtmlCrawler().getDocument("0135157862", "new");
		
	}
	
	@Test 
	public void testParser() {
		List<Offer> offers = htmlParser.parseOffer(document);
		assertEquals(10, offers.size());
	}

}
