package edu.olivet.se530;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.olivet.se530.dummy.DummyHtmlCrawler;
import edu.olivet.se530.model.Seller;

public class SellerParserTest {
	static Elements offerRows;
	static Element APOffer;
	static Element commonOffer;
	@BeforeClass
	public static void init() throws MalformedURLException, IOException {
		HtmlCrawler crawler = new DummyHtmlCrawler();
		Document doc = crawler.getDocument("135157862", "new");
		offerRows = doc.select("div.a-row.a-spacing-mini.olpOffer");
		APOffer = offerRows.get(0);
		commonOffer = offerRows.get(1);
	}
	

	@Test
	public void testIsAPSeller() {
		assertTrue(SellerParser.isAPSeller(APOffer));
		assertFalse(SellerParser.isAPSeller(commonOffer));
	}

	@Test
	public void testAPSeller() {
		assertTrue(SellerParser.parse(APOffer).getName().equals("AP"));
		assertTrue(SellerParser.parse(APOffer).getRating() > 90);
		assertTrue(SellerParser.parse(APOffer).getRatingCount() > 100);
		assertTrue(SellerParser.parse(APOffer).getUuid().equals("AP"));
	}

	@Test
	public void testParseSellerName() {
		assertEquals("collegebooksdirect", SellerParser.parseSellerName(commonOffer));
	}

	@Test
	public void testParseUuid() {
		assertEquals("A2DIRKU079H2QT", SellerParser.parseUuid(commonOffer));
	}

	@Test
	public void testParseRating() {
		assertEquals(94, SellerParser.parseRating(commonOffer));
	}

	@Test
	public void testParseRatingCount() {
		assertEquals(217514, SellerParser.parseRatingCount(commonOffer));
	}
	
	public void testParse() {
		Seller commonSeller = SellerParser.parse(commonOffer);
		assertEquals("A2DIRKU079H2QT", commonSeller.getUuid());
		assertEquals("collegebooksdirect", commonSeller.getName());
		assertEquals("United States", commonSeller.getShippingCountry());
		assertEquals("TX", commonSeller.getShippingState());
		assertEquals(94, commonSeller.getRating());
		assertEquals(217514, commonSeller.getRatingCount());
	}

}
