package edu.olivet.se530;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public final class SHUtil {
	
	public static String getText(Element element, String selector) {
		Elements elements = element.select(selector);
		if (elements.size() <= 0) {
			return "";
		}
		return elements.get(0).text();
	}
	
	public static String formatIsbn(String isbn, int isbnLenght) {
		if (isbn.length() < isbnLenght) {
			StringBuilder formatedIsbn = new StringBuilder(isbn);
			for (int i = isbnLenght; i > isbn.length(); i--) {
				formatedIsbn.insert(0, '0');
			}
			return formatedIsbn.toString();
		}else {
			return isbn;
		}	
	}
	
}
