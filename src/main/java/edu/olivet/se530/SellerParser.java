package edu.olivet.se530;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.olivet.se530.model.Seller;

public class SellerParser {
	private final static String sellerNameSelector = "p.a-spacing-small.olpSellerName";
	
	public static boolean isAPSeller(Element offerRow) {
		return offerRow.select(sellerNameSelector + " a").size() == 0;
	}
	
	public static Seller APSeller() {
		Seller seller = new Seller();
		seller.setName("AP");
		seller.setUuid("AP");
		seller.setRating(98);
		seller.setRatingCount(99999);
		return seller;
	} 
	
	public static String parseSellerName(Element offerRow) {
		return SHUtil.getText(offerRow, sellerNameSelector);
	}
	
	public static String parseUuid(Element offerRow) {
		String link = offerRow.select(sellerNameSelector + " a").get(0).attr("href");
		return link.replaceFirst(".*&seller=", "");
	}
	
	public static int parseRating(Element offerRow) {
		String ratingText = SHUtil.getText(offerRow, "p.a-spacing-small > a > b");
		int rating = Integer.parseInt(ratingText.replaceAll("[^0-9]", ""));
		return rating;
	}
	
	public static int parseRatingCount(Element offerRow) {
		String ratingCountText = SHUtil.getText(offerRow, "div.a-column.a-span2.olpSellerColumn > p:nth-child(2)");
		ratingCountText = ratingCountText.substring(ratingCountText.indexOf('('), ratingCountText.indexOf(')')).replaceAll("[^0-9]", "");
		return Integer.parseInt(ratingCountText);
	}
	
	public static void setShipping(Seller seller, Element offerRow) {
		Elements deliveries = offerRow.select("ul.a-vertical > li > span.a-list-item");
		for (int j = 0; j < deliveries.size(); j++) {
			String text = deliveries.get(j).text();
			if (text.contains("Expedited shipping available")) {
				seller.setExpeditedShippingAvailable(true);
			}
			if (text.contains("International & domestic shipping rates and return policy")) {
				seller.setIntlShippingAvailable(true);
			}
			if (text.matches("Ships from [A-Z]{2}, United States[.]")) {
				String[] array = text.replace("Ships from", "").split(",");
				seller.setShippingState(array[0].trim());
				seller.setShippingCountry(array[1].trim().replace(".", ""));
			}else if(text.contains("Ships from United Kingdom")) {
				seller.setShippingCountry("United Kingdom");
			}
		}
	}
	
	public static Seller parse(Element offerRow) {
		Seller seller = new Seller();
		if(isAPSeller(offerRow)) {
			seller = APSeller();
		}else {
			seller.setName(parseSellerName(offerRow));
			seller.setUuid(parseUuid(offerRow));
			seller.setRating(parseRating(offerRow));
			seller.setRatingCount(parseRatingCount(offerRow));
		}	
		setShipping(seller, offerRow);
		
		return seller;
	}
	
}

