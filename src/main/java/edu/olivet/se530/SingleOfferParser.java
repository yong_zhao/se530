package edu.olivet.se530;

import org.jsoup.nodes.Element;

import edu.olivet.se530.model.Condition;
import edu.olivet.se530.model.Offer;

/**
 * parse a single offer row to Offer object
 * @author yzhao
 * 
 */
public class SingleOfferParser {
	
	public static Offer parse(Element offerRow) {
		Offer offer = new Offer();
		offer.setPrice(parsePrice(offerRow));
		offer.setShippingPrice(parseShippingPrice(offerRow));
		offer.setCondition(parseCondition(offerRow));
		offer.setSeller(SellerParser.parse(offerRow));
		return offer;
	}
	
	public static Float parsePrice(Element offerRow) {
		return Float.parseFloat(SHUtil.getText(offerRow, "span.olpOfferPrice").replace("$", ""));
	}
	
	public static Float parseShippingPrice(Element offerRow) {
		String shippingFeeText = SHUtil.getText(offerRow, "span.olpShippingPrice").replace("$", "");
		if (shippingFeeText != null && shippingFeeText.trim().length() > 0) {
			return Float.parseFloat(shippingFeeText);
		}
		return (float) 0;
	}
	
	public static Condition parseCondition(Element offerRow) {
		String cond = SHUtil.getText(offerRow, "h3.a-spacing-small.olpCondition");
		Condition condition = new Condition();
		if (cond.trim().toLowerCase().equals("new")) {
			condition.setPrimary(cond.trim());
		}else {
			String[] array = cond.split("-");
			condition.setPrimary(array[0].trim());
			condition.setSecondary(array[1].trim());
		}
		return condition;
	}
	
}
