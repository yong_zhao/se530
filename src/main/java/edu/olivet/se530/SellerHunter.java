package edu.olivet.se530;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.jsoup.nodes.Document;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import edu.olivet.se530.model.Offer;
import edu.olivet.se530.model.Product;

@Singleton
public class SellerHunter {
	@Inject private HtmlCrawler htmlFetcher;
	@Inject private HtmlParser htmlParser;
	private static final int RatingTreshold = 95;
	private static final int RatingCountTreshold = 100;
	private static final String InvalidCountry = "United Kingdom";
	/**
	 * 根据给定的isbn和condition，返回亚马逊网站上面的Offer列表
	 * @param isbn		产品的ISBN编号，参见:{@link Product#getIsbn()}
	 * @param condition	产品的Condition
	 */
	public Offer huntOffer(String isbn, String condition) throws MalformedURLException, IOException {
		Document doc = htmlFetcher.getDocument(isbn, condition);
		List<Offer> offers = htmlParser.parseOffer(doc);
		
		for (Iterator<Offer> iterator = offers.iterator(); iterator.hasNext();) {
			Offer offer = iterator.next();
			if (!this.evalute(offer)) {
				iterator.remove();
			}
		}
		Collections.sort(offers);
		return offers.get(0);
	}
	/**
	 * 对一个Offer按照价格、运费、Rating等等标准进行审查
	 * 1.Rating必须在95以上
	 * 2.Rating数必须在100以上
	 * 3.不能从英国发货
	 * @param offer
	 */
	public boolean evalute(Offer offer) {
		return this.evaluateRating(offer) &&
				this.evaluateRatingCount(offer) &&
				this.evaluateShippingCountry(offer);
	}
   
	
	private boolean evaluateRatingCount(Offer offer) {
		return offer.getSeller().getRatingCount() >= RatingCountTreshold;
	}
	
	private boolean evaluateRating(Offer offer) {
		return offer.getSeller().getRating() >= RatingTreshold;
	}
	
	private boolean evaluateShippingCountry(Offer offer) {
		return !InvalidCountry.equalsIgnoreCase(offer.getSeller().getShippingCountry());
	}
	
	@Inject
	public void setHtmlFetcher(HtmlCrawler htmlFetcher) {
		this.htmlFetcher = htmlFetcher;
	}
	
	@Inject
	public void setHtmlParser(HtmlParser htmlParser) {
		this.htmlParser = htmlParser;
	}
}