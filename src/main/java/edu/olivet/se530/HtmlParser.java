package edu.olivet.se530;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.olivet.se530.annotations.Profile;
import edu.olivet.se530.model.Offer;

public class HtmlParser {
	
	@Profile(desc = "解析一个给定的html document，返回其中的offer列表")
	public List<Offer> parseOffer(Document doc) {
		List<Offer> results = new ArrayList<Offer>();
		Elements rows = doc.select("div.a-row.a-spacing-mini.olpOffer");
		for (int i = 0; i < rows.size(); i++) {
			Element row = rows.get(i);
			Offer offer = SingleOfferParser.parse(row);
			results.add(offer);
		}
		return results;
	}
}
